package main

import (
	"encoding/json"
	"log"
	"net/url"
	"time"

	"github.com/gorilla/websocket"
)

type MachineStreamListener struct {
	uri  url.URL
	repo *Repository
}

type EventPayload struct {
	MachineID string    `json:"machine_id"`
	EventID   string    `json:"id"`
	Timestamp time.Time `json:"timestamp"`
	Status    string    `json:"status"`
}

type EventPacket struct {
	Topic   string
	Ref     string
	Payload EventPayload
	Event   string
}

func (l *MachineStreamListener) processMessage(message []byte) {
	var eventData EventPacket
	err := json.Unmarshal([]byte(message), &eventData)
	if err != nil {
		log.Println("parse error:", err)
	}
	log.Printf("received message: %s", eventData)

	payload := eventData.Payload

	l.repo.UpsertMachine(&MachineEntity{
		ID:                    payload.MachineID,
		Status:                payload.Status,
		LastActivityTimestamp: payload.Timestamp,
	})

	l.repo.UpsertEvent(&EventEntity{
		ID:        payload.EventID,
		MachineID: payload.MachineID,
		Status:    payload.Status,
		Timestamp: payload.Timestamp,
	})
}

func InitMachineStreamListener(uri url.URL, repo *Repository) *MachineStreamListener {
	return &MachineStreamListener{
		uri:  uri,
		repo: repo,
	}
}

func (l *MachineStreamListener) Run() {
	reconnect := make(chan struct{})
	defer close(reconnect)
	go func() { reconnect <- struct{}{} }()

	var c *websocket.Conn
	defer c.Close()

	var err error

	for {
		select {

		case <-reconnect:
			c, _, err = websocket.DefaultDialer.Dial(l.uri.String(), nil)
			if err != nil {
				log.Println("dial:", err)
				select {
				case <-time.After(time.Second):
					reconnect <- struct{}{}
				}
			} else {
				go func() {
					for {
						_, message, err := c.ReadMessage()
						if err != nil {
							log.Println("read error:", err)
							reconnect <- struct{}{}
							return
						}

						l.processMessage(message)
					}
				}()
			}
		}
	}
}
