package main

import (
	"log"
	"net/http"
	"net/url"
	"os"

	"github.com/gorilla/mux"
)

func main() {
	log.Println("Fiat lux!")

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Println("Using port: ", port)

	dbURI := os.Getenv("MONGODB_URI")
	if dbURI == "" {
		dbURI = "mongodb://localhost/homework-zeiss"
	}
	log.Println("Using mongodb URI: ", dbURI)

	repo, err := InitRepository(dbURI)
	if err != nil {
		log.Fatal("Init repository failed: ", err)
	}

	listener := InitMachineStreamListener(url.URL{Scheme: "ws", Host: "machinestream.herokuapp.com", Path: "/ws"}, repo)

	r := mux.NewRouter()
	api := InitAPI(port, repo, r)

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))

	go listener.Run()
	api.Run()
}
