package main

import (
	"context"
	"log"
	"math"
	"net/url"
	"sort"
	"strings"
	"time"

	"github.com/mongodb/mongo-go-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Repository represents the repository of machine events
type Repository struct {
	mongoDbURI string
	client     *mongo.Client
	db         *mongo.Database
}

// MachineEntity contains machine related data
type MachineEntity struct {
	ID                    string
	Status                string
	LastActivityTimestamp time.Time
}

// EventEntity contains event related data
type EventEntity struct {
	ID        string
	MachineID string
	Timestamp time.Time
	Status    string
}

// InitRepository performs repository initialization
func InitRepository(uri string) (*Repository, error) {
	parsedURI, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}
	dbName := strings.Trim(parsedURI.Path, "/")
	log.Println("Resovled DB name:", dbName)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, uri)
	if err != nil {
		return nil, err
	}
	database := client.Database(dbName)
	repository := &Repository{
		mongoDbURI: uri,
		client:     client,
		db:         database,
	}

	return repository, nil
}

func (r *Repository) getEntities(collectionName string, filter interface{}, read func(cur *mongo.Cursor) error) error {
	collection := r.db.Collection(collectionName)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cur, err := collection.Find(ctx, filter)
	if err != nil {
		return err
	}

	defer cur.Close(ctx)

	for cur.Next(ctx) {
		if err := read(cur); err != nil {
			return err
		}
	}
	if err := cur.Err(); err != nil {
		return err
	}

	return nil
}

// GetMachines deliveres the list of machines
func (r *Repository) GetMachines() ([]MachineEntity, error) {
	entities := []MachineEntity{}
	err := r.getEntities("machines", bson.D{}, func(cur *mongo.Cursor) error {
		var result MachineEntity
		if err := cur.Decode(&result); err != nil {
			return err
		}
		entities = append(entities, result)
		return nil
	})
	return entities, err
}

// GetMachineByID gets the machine data by the machine ID
func (r *Repository) GetMachineByID(id string) (*MachineEntity, error) {
	entities := []MachineEntity{}
	err := r.getEntities("machines", bson.M{"id": id}, func(cur *mongo.Cursor) error {
		var result MachineEntity
		if err := cur.Decode(&result); err != nil {
			return err
		}
		entities = append(entities, result)
		return nil
	})
	if len(entities) > 0 {
		return &entities[0], err
	}

	return nil, err
}

// UpsertMachine creates/updates the machine data
func (r *Repository) UpsertMachine(entity *MachineEntity) error {

	collection := r.db.Collection("machines")

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	existing := MachineEntity{}
	filter := bson.M{"id": entity.ID}
	err := collection.FindOne(ctx, filter).Decode(&existing)
	if err == nil {
		existing.Status = entity.Status
		existing.LastActivityTimestamp = entity.LastActivityTimestamp
		_, err := collection.ReplaceOne(ctx, filter, existing)
		return err
	} else {
		log.Print("existing not found: ", err)
		_, err := collection.InsertOne(ctx, entity)
		return err
	}
}

// GetMachineEvents deliveres the list of machine events
func (r *Repository) GetMachineEvents(machineID string) ([]EventEntity, error) {
	entities := []EventEntity{}
	err := r.getEntities("events", bson.M{"machineid": machineID}, func(cur *mongo.Cursor) error {
		var result EventEntity
		if err := cur.Decode(&result); err != nil {
			return err
		}
		entities = append(entities, result)
		return nil
	})
	return entities, err
}

// GetEvents deliveres the list of all events
func (r *Repository) GetEvents(limit int) ([]EventEntity, error) {
	entities := []EventEntity{}
	err := r.getEntities("events", bson.D{}, func(cur *mongo.Cursor) error {
		var result EventEntity
		if err := cur.Decode(&result); err != nil {
			return err
		}
		entities = append(entities, result)
		return nil
	})

	sort.Slice(entities[:], func(i, j int) bool {
		return entities[i].Timestamp.Unix() > entities[j].Timestamp.Unix()
	})

	limit = int(math.Min(float64(limit), float64(len(entities))))
	return entities[:limit], err
}

// GetEventByID gets the event data by the event ID
func (r *Repository) GetEventByID(id string) (*EventEntity, error) {
	entities := []EventEntity{}
	err := r.getEntities("events", bson.M{"id": id}, func(cur *mongo.Cursor) error {
		var result EventEntity
		if err := cur.Decode(&result); err != nil {
			return err
		}
		entities = append(entities, result)
		return nil
	})
	if len(entities) > 0 {
		return &entities[0], err
	}

	return nil, err
}

// UpsertEvent creates/updates the event data
func (r *Repository) UpsertEvent(entity *EventEntity) error {

	collection := r.db.Collection("events")

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	existing := EventEntity{}
	filter := bson.M{"id": entity.ID}
	err := collection.FindOne(ctx, filter).Decode(&existing)
	if err == nil {
		existing.Status = entity.Status
		existing.Timestamp = entity.Timestamp
		_, err := collection.ReplaceOne(ctx, filter, existing)
		return err
	} else {
		log.Print("existing not found: ", err)
		_, err := collection.InsertOne(ctx, entity)
		return err
	}
}
