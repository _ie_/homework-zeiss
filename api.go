package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type API struct {
	port   string
	repo   *Repository
	router *mux.Router
}

type MachineInfo struct {
	ID                    string    `json:"machine_id"`
	Status                string    `json:"last_status"`
	LastActivityTimestamp time.Time `json:"last_activity_timestamp"`
}

type MachineEvent struct {
	ID        string    `json:"id"`
	MachineID string    `json:"machine_id"`
	Timestamp time.Time `json:"timestamp"`
	Status    string    `json:"status"`
}

func (entity *MachineEntity) toMachineInfo() *MachineInfo {
	return &MachineInfo{
		ID:                    entity.ID,
		Status:                entity.Status,
		LastActivityTimestamp: entity.LastActivityTimestamp,
	}
}

func (entity *EventEntity) toMachineEvent() *MachineEvent {
	return &MachineEvent{
		ID:        entity.ID,
		MachineID: entity.MachineID,
		Status:    entity.Status,
		Timestamp: entity.Timestamp,
	}
}

func getMachines(a *API, w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	entities, err := a.repo.GetMachines()
	if err != nil {
		return err
	}

	var machines []MachineInfo
	for _, entity := range entities {
		machines = append(machines, *entity.toMachineInfo())
	}
	json.NewEncoder(w).Encode(machines)
	return nil
}

func getMachineByID(a *API, w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	machineID := mux.Vars(r)["machine_id"]
	entity, err := a.repo.GetMachineByID(machineID)
	if err != nil {
		return err
	}
	if entity == nil {
		http.NotFound(w, r)
	} else {
		json.NewEncoder(w).Encode(entity.toMachineInfo())
	}
	return nil
}

func getMachineEvents(a *API, w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	entities, err := a.repo.GetMachineEvents(mux.Vars(r)["machine_id"])
	if err != nil {
		return err
	}

	var events []MachineEvent
	for _, entity := range entities {
		events = append(events, *entity.toMachineEvent())
	}
	json.NewEncoder(w).Encode(events)
	return nil
}

func getEvents(a *API, w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	limits, ok := r.URL.Query()["limit"]
	var limit = int(^uint(0) >> 1)
	if ok && len(limits) > 0 {
		if l, err := strconv.Atoi(limits[0]); err == nil {
			limit = l
		}
	}

	entities, err := a.repo.GetEvents(limit)
	if err != nil {
		return err
	}

	var events []MachineEvent
	for _, entity := range entities {
		events = append(events, *entity.toMachineEvent())
	}
	json.NewEncoder(w).Encode(events)
	return nil
}

func getEventByID(a *API, w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	entity, err := a.repo.GetEventByID(mux.Vars(r)["event_id"])
	if err != nil {
		return err
	}
	if entity == nil {
		http.NotFound(w, r)
	} else {
		json.NewEncoder(w).Encode(entity.toMachineEvent())
	}
	return nil
}

func InitAPI(port string, repo *Repository, router *mux.Router) *API {
	api := API{
		port:   port,
		repo:   repo,
		router: router,
	}

	for route, action := range routeHandlers {
		closureAction := action
		router.HandleFunc(route, func(w http.ResponseWriter, r *http.Request) {
			err := closureAction(&api, w, r)
			if err != nil {
				log.Print("Failed request processing: ", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("500 - Please try again later"))
			}
		}).Methods("GET")
	}

	return &api
}

// Run starts the API calls handling
func (a *API) Run() {
	log.Fatal(http.ListenAndServe(":"+a.port, a.router))
}

var routeHandlers = map[string]func(a *API, w http.ResponseWriter, r *http.Request) error{
	"/api/machines":                     getMachines,
	"/api/machines/{machine_id}":        getMachineByID,
	"/api/machines/{machine_id}/events": getMachineEvents,
	"/api/events":                       getEvents,
	"/api/events/{event_id}":            getEventByID,
}
